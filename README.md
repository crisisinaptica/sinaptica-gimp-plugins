# gimp-plugins
Some GIMP plugins writen in C and Python.

All plugins compiled and tested in **Linux** with **GIMP >= 2.10.6**

## Plugin description.

### ~ Python

#### gradient_from_palette
This plugin will create a gradient using the palette colors in the range specified the first index and last index parameters.

Several gradient types can be generated (Smooth, Cyclic and Discrete)

· Smooth: gradient each segment of the gradient is a smooth transition between two colors.

· Cyclic: A smooth gradient with one extra segment added at the end, to chain the last color to the first one.

· Discrete: Each segment is a solid mass of color.

The gradient can be optionally inverted and ordered based on several criteria (luminance, value, hue, saturation, etc...)

By installing this plugin the existent processes provided by `palette_to_gradient.py' will be overriden by this implementation.

#### automation
A collection of python plugins to implement macros and the automation of workflows in GIMP.

This is a heavily modified fork of the scripts published by Stephen Kiel in his fantastic [Automate Editing GIMP tutorial](https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/).

Read the tutorial for an extensive explanation of the whole system.

Note to self: You need to write proper documentation for this implementation at some point. ;)

#### doom_sprite_composer
This script is designed to process the render output (individual sprites and several masks for each one) of a Blender project I use to render all the needed camera views of a 3d model.

It's an important part in my sprite creation pipeline.

At some point I will release the rest of the required components (the Blender project and a python script) and explain how everything works in detail.

### save_all
Will save all currently opened images (If they already have a XCF file associated)

### watermark
A plugin to create a simple watermark layer.

#### pyzealous
A python version of [ZealousCrop](https://github.com/GNOME/gimp/blob/0ecd936f576768070ead4e78011635031020af79/plug-ins/common/crop-zealous.c), writen to teach myself how to create a gimp python plugin while I studied the C source of the original plugin.

It's very slow when compared to the original, probably crap and not of much use for anything, but maybe it's of someone interest, so I'll leave it here.


### ~ C

#### crop-zealous
A modified version of [ZealousCrop](https://docs.gimp.org/en/plug-in-zealouscrop.html):

The plugin will analyze the active drawable and, based on color/alpha similarities, will mark rows and columns of pixels to keep/discard.

Supported drawables for analysis are bitmap layers, layer masks and channels.

If the padding value is greater than zero, areas marked to keep will be expanded in all directions a number of rows/columns equal to padding, predating the areas marked to be discarded.
This means padding will not add extra blank space non existent in the source, instead it works protecting blank lines and rows founded in the analyzed drawable.
The padding value is thus limited to the number of blank rows and columns found between the elements identified as 'content'.

If 'all_layers' is true, the plugin will rearrange the contents of all layers, masks and channels based on the analysis of the active item.

If not, it will only act upon the active drawable, leaving all others as they were.

## Install

#### 1. Clone the repository:
```
# git clone https://github.com/crisisinaptica/gimp-plugins.git
```

#### 2.a Use the gimptool-2.0 command to install the plugins.
```
# gimptool-2.0 --help
```

#### 2.b Use the install.py script (*nix)

Compile/Install all plugins:
```
# cd gimp-plugins
# python install.py install
```

If one several plugin names are passed as arguments to the script, the operations will only be executed for those plugins.
```
# python install.py install crop-zealous
```
The operation modes are:
1. **list**: List available plugins.
2. **install**: Build and/or copy the plugin to the GIMP plug-ins folder.
3. **uninstall**: Delete the installed files.
4. **link**: Populate the GIMP plug-ins folder with symbolic links to the (python) sources and (C) binaries on the local repo. 
5. **unlink**: Remove the symbolic links.

