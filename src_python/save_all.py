# -*- coding: utf-8 -*-

"""
save_all GIMP plug-in

Saves all currently opened images associated to a XCF file.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from os import path
from gimpfu import *

__author__    = 'Sergio Jiménez Herena'
__copyright__ = '2018, Sergio Jiménez Herena'
__license__   = 'GPL3'


def save_all_xcf():
    images = [i for i in gimp.image_list()
              if path.splitext(i.filename)[1].lower() == '.xcf']

    for image in images:
        pdb.gimp_xcf_save('>9000',
                          image,
                          image.active_drawable,
                          image.filename,
                          image.filename)
        pdb.gimp_image_clean_all(image)


register(
    'python-fu-save-all',
    'Saves all currently opened images associated to a XCF file.',
    'Saves all currently opened images associated to a XCF file.',
    __author__,
    __author__,
    '2018',
    'Save all',
    '*',
    [],
    [],
    save_all_xcf,
    menu='<Image>/File/Save'
)

main()
