#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_import_img.py
    by Sinaptica.

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from auto_base import *

__author__    = 'Sergio Jiménez Herena'
__copyright__ = 'Copyright 2018, Sergio Jiménez Herena'
__license__   = 'GPL3'

try:
    flow_read = AutoFlowReader()
    flow_list = flow_read.get_names()
except IOError:
    flow_list = []


def auto_import_img(src_dir, tgt_dir, bpp, profile, intent, bpc, flow_index,
                    compress):
    """Open images from src_dir and saves them as xcf images in the tgt_dir

    Arguments:
    src_dir (string,path): Source directory path.
    tgt_dir (string,path): Target directory path.
    flow_index (string): Work-flow name.
    """

    prop_read = AutoPropReader()

    pdb.gimp_plugin_enable_precision()
    open_images = pdb.gimp_image_list()[0]

    if open_images > 0:
        pdb.gimp_message('Close open Images & Rerun')

    else:
        flow_name = flow_list[flow_index]
        flow_step_dict = flow_read.first_name_dict()
        next_step = flow_step_dict[flow_name]

        prop_list = prop_read.get_default_options()
        prop_list.append(('flow', flow_name))
        prop_list.append(('next_step', next_step))
        prop_list.append(('current_step', 'first'))

        existing_list = listdir(tgt_dir)
        src_file_list = [f for f in listdir(src_dir) if
                         path.splitext(f)[1].lower() in
                         DEFAULTS['ext']]
        tgt_file_list = ['{}.{}'.format(path.splitext(f)[0], 'xcf')
                         for f in src_file_list]
        tgt_file_dict = dict(zip(src_file_list, tgt_file_list))

        for src_file in src_file_list:

            tgt_path = path.join(tgt_dir, tgt_file_dict[src_file])
            src_path = path.join(src_dir, src_file)

            if tgt_file_dict[src_file] not in existing_list:
                img = pdb.gimp_file_load(src_path, src_file)
            else:
                src_path = tgt_path
                img = pdb.gimp_file_load(src_path, src_file)
                overwrite_flag = img.parasite_find('overwrite_xcf')
                if not bool(int(overwrite_flag.data)):
                    pdb.gimp_image_delete(img)
                    continue

            if img.base_type != INDEXED:
                if not profile:
                    prof_key = 'rgb_working'
                    if img.base_type == GRAY:
                        prof_key = 'gray_working'
                    if bpp % 2 == 0:
                        prof_key = '{}_{}'.format(prof_key, 'gamma')
                    else:
                        prof_key = '{}_{}'.format(prof_key, 'linear')
                    profile = DEFAULTS['icc'][prof_key]  # May be None.
                if profile:
                    pdb.plug_in_icc_profile_apply(img, profile, intent, bpc)
                if pdb.gimp_image_get_precision(img) != PRECISION[bpp]:
                    pdb.gimp_image_convert_precision(img, PRECISION[bpp])

            for prop_name, prop_val in prop_list:
                img.attach_new_parasite(prop_name, PARAS_IDX, prop_val)

            pdb.gimp_xcf_save(0, img, img.active_drawable,
                              tgt_path, tgt_file_dict[src_file])
            pdb.gimp_image_delete(img)

        pdb.python_fu_auto_auto_update_dir(tgt_dir, compress)


register(
    'python_fu_auto_import_img',
    'Import images to xcf',
    'Import images to xcf',
    'Sergio Jiménez Herena',
    'Sergio Jiménez Herena',
    'March 2018',
    '1) Import images to XCF',
    '',
    [
        (PF_DIRNAME, 'src_path',
         'Source images directory:',
         DEFAULTS['dir']),
        (PF_DIRNAME, 'tgt_path',
         'Target images directory:',
         DEFAULTS['dir']),
        (PF_OPTION, 'bpp', 'Promote precision',
         DEFAULTS['import_bpp'],
         ('Unsigned 8bpp sRGB', 'Unsigned 8bpp Linear',
          'Unsigned 16bpp sRGB', 'Unsigned 16bpp Linear',
          'Unsigned 32bpp sRGB', 'Unsigned 32bpp Linear',
          'Float 16bpp sRGB', 'Float 16bpp Linear',
          'Float 32bpp sRGB', 'Float 32bpp Linear')),
        (PF_FILE, 'profile', 'Working color profile', None),
        (PF_OPTION, 'intent', 'Rendering intent',
         0, ('Perceptual',
             'Relative Colorimetric',
             'Saturation',
             'Absolute Colorimetric')),
        (PF_BOOL, 'bpc', 'Black Point Compensation', False),
        (PF_OPTION, 'flow_index', 'Select Workflow', 0, flow_list),
        (PF_BOOL, 'compress', 'Compress XCF files', False)
    ],
    [],
    auto_import_img,
    menu='<Image>/Automation'
)
