flow>FullAuto

step>Alignment

    comment> Set up the grid for Rotate and or Perspective Transform
    comment> Easier to do geometric operations with single layer
    comment> This first step is run as part of the import jpeg
    comment> In the console get ID with: img = gimp.image_list()[0]
    comment> First set some variables and establish grid spacing and style

    >>> center_x = img.width // 2
    >>> center_y = img.height // 2
    >>> grid_spacing = max(img.width, img.height) // 24
    >>> pdb.gimp_image_grid_set_offset(img, center_x, center_y)
    >>> pdb.gimp_image_grid_set_spacing(img, grid_spacing, grid_spacing)
    >>> pdb.gimp_image_grid_set_style(img, GRID_ON_OFF_DASH)

    comment> Expand the canvas by 25 percent, easier transforms.

    >>> adjust = max(img.height, img.width) // 4
    >>> img.resize(img.width + adjust, img.height+ adjust, adjust // 2, adjust // 2)

    comment> Rename the base layer to 'original'

    >>> layer = img.layers[0]
    >>> layer.name = 'original'

    comment> Shrink the Canvas back to fit the layer

    >>> img.resize_to_layers()

    comment> Set grid to origin and size = img

    >>> pdb.gimp_image_grid_set_offset(img, 0, 0)
    >>> pdb.gimp_image_grid_set_spacing(img, img.width, img.height)

    comment> Capture a new color layer.
    comment> Move it to the bottom of the stack.
    comment> Save 'color' so we can recover it at the end of the flow
    comment> We don't have to worry if some of the filters skew the color.
    comment> We can also use this to amp up the color

    >>> img.active_layer = img.layers[0]
    >>> current_layer = img.active_layer
    >>> temp_layer = current_layer.copy()
    >>> img.add_layer(temp_layer, 0)
    >>> pdb.gimp_drawable_desaturate(temp_layer, DESATURATE_LUMA)
    >>> temp_layer.mode = LAYER_MODE_GRAIN_EXTRACT
    >>> color_layer = pdb.gimp_layer_new_from_visible(img, img, 'color_layer')
    >>> img.add_layer(color_layer, len(img.layers))
    >>> color_layer.mode = LAYER_MODE_GRAIN_MERGE_MODE
    >>> img.remove_layer(temp_layer)
    >>> img.active_layer = img.layers[0]

    comment> Create a Dynamic Range Layer and add it at the top of the stack
    comment> Run auto stretch_hsv leave mode Normal and at 100% opacity
    comment> May have to use 'curves' to tune up histogram

    >>> original = img.active_layer
    >>> dyn_range = original.copy()
    >>> dyn_range.name = 'dyn_range'
    >>> img.add_layer(dyn_range, 0)
    >>> pdb.plug_in_autostretch_hsv(img, dyn_range)

    comment> Add a new retinex layer and apply the retinex filter with default settings

    >>> retinex_layer = pdb.gimp_layer_new_from_visible(img, img, 'retinex')
    >>> img.add_layer(retinex_layer, 0)

    comment> Retinex parameters - Scale = 240; ScaleDiv = 3; Level = 0; Dynamic = 1.2

    >>> pdb.plug_in_retinex(img, retinex_layer, 240, 3, 0, 1.2)

    comment> Make the retinex layer B&W - retinex distorts color

    >>> pdb.gimp_drawable_desaturate(retinex_layer, DESATURATE_LUMA)
    >>> retinex_layer.mode = LAYER_MODE_OVERLAY

    comment> Adjust the opacity based on parasite enhance_contrast_level

    >>> retinex_layer.opacity = float(img.parasite_find('enhance_contrast_level').data)

    comment> Add a new sharpen layer and apply the sharpening filter with default settings

    >>> sharpen_layer = pdb.gimp_layer_new_from_visible(img, img, 'sharpen')
    >>> img.add_layer(sharpen_layer, 0)

    comment> Sharpen parameters - Radius = 5.0; Amount = 0.5; Threshold = 0

    >>> pdb.plug_in_unsharp_mask(img, sharpen_layer, 5.0, 0.5, 0)

    comment> Adjust the opacity based on parasite enhance_contrast_level

    >>> sharpen_layer.opacity = float(img.parasite_find('enhance_contrast_level').data)

    comment> Merge the work layers together and raise the color layer to the top
    comment> First the retinex layer

    >>> retinex_layer = pdb.gimp_image_get_layer_by_name(img, 'retinex')
    >>> temp_layer = pdb.gimp_image_merge_down(img, retinex_layer, EXPAND_AS_NECESSARY)

    comment> Then the sharpen Layer

    >>> sharpen_layer = pdb.gimp_image_get_layer_by_name(img, 'sharpen')
    >>> new_layer = pdb.gimp_image_merge_down(img, sharpen_layer, EXPAND_AS_NECESSARY)
    >>> new_layer.name = 'contrast'

    comment> Desaturate the resultant layer

    >>> pdb.gimp_drawable_desaturate(new_layer, DESATURATE_LUMA)

    comment> Now grab the color layer and move it to the top

    >>> color_layer = pdb.gimp_image_get_layer_by_name(img, 'color_layer')
    >>> pdb.gimp_image_raise_item_to_top(img, color_layer)
    >>> color_layer.opacity = 80.0

    comment> Copy the Color Layer, call it color_add, set opacity to 20%.  Adjust by hand as needed.

    >>> color_add = pdb.gimp_layer_copy(color_layer, FALSE)
    >>> img.add_layer(color_add, 0)
    >>> color_add.name = 'color_add'

    comment> Adjust the Color Layer opacity based on parasite enhance_color_level

    >>> color_add.opacity = float(img.parasite_find('enhance_color_level').data)