#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_export_img.py
    by Sinaptica.

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import division
from auto_base import *

__author__    = 'Sergio Jiménez Herena'
__copyright__ = 'Copyright 2018, Sergio Jiménez Herena'
__license__   = 'GPL3'


class AutoImageExporter(object):

    def __init__(self, options):
        """Base class for all image exporters.

        Parameters:
        options (dict): Export options.
        """

        self._src_dir = options['src_dir']
        self._tgt_dir = options['tgt_dir']

        if options['tgt_dir'] is None:
            self._tgt_dir = path.join(self._src_dir, 'exported')
        else:
            self._tgt_dir = options['tgt_dir']

        if not path.isdir(self._tgt_dir):
            makedirs(self._tgt_dir, mode=0764)

        self._src_files = listdir(self._src_dir)
        self._tgt_files = listdir(self._tgt_dir)
        self._opt       = options

    def __resize(self, img):
        """Limit the image size to a maximum width and height.

        The aspect ratio of the image will be preserved.

        Arguments:
        img (gimp.Image): The image that will be scaled.
        """

        max_w = self._opt['max_w']
        max_h = self._opt['max_h']
        img_new_w = img_w = img.width
        img_new_h = img_h = img.height

        if img_w <= max_w and img_h <= max_h:
            return
        else:
            if max_w == 0:
                max_w = img_w
            if max_h == 0:
                max_h = img_h
            img_ratio = img_w / img_h
            if img_w > max_w:
                img_new_w = max_w
                img_new_h = img_new_w / img_ratio
                if img_new_h > max_h:
                    img_new_h = max_h
                    img_new_w = img_new_h * img_ratio
            elif img_h > max_h:
                img_new_h = max_h
                img_new_w = img_new_h * img_ratio
                if img_new_w > max_w:
                    img_new_w = max_w
                    img_new_h = img_new_w / img_ratio

            pdb.gimp_image_scale(img, round(img_new_w), round(img_new_h))

    def _export(self, img, drawable, tgt_path):
        """Overridden in the format specific sub-classes."""
        pass

    def run(self):
        """Process the sources directory."""

        max_w = self._opt['max_w']
        max_h = self._opt['max_h']
        ext = self._opt['ext']
        bpp = self._opt['bpp']
        icc = self._opt['profile']
        intent = self._opt['intent']
        bpc = self._opt['bpc']
        strip_meta = self._opt['strip_meta']

        pdb.gimp_plugin_enable_precision()

        comp = DEFAULTS['compression']

        src_file_list = [f for f in self._src_files if
                         path.splitext(f)[1].lower() == '.xcf' or
                         path.splitext(f)[1].lower() == '.xcf' + comp]
        tgt_file_list = ['{}.{}'.format(path.splitext(f)[0], ext)
                         for f in src_file_list]

        files_dict = dict(zip(src_file_list, tgt_file_list))

        for file_name in src_file_list:
            do_it = False
            src_file_path = path.join(self._src_dir, file_name)
            img = pdb.gimp_file_load(src_file_path, src_file_path)
            drawable = img.active_drawable
            if str(img.parasite_find('current_step')) == 'FINISHED':
                if str(img.parasite_find('overwrite_file')) == 'YES':
                    do_it = True
                    img.attach_new_parasite('overwrite_file', PARAS_IDX, 'NO')
                    pdb.gimp_xcf_save(0, img, drawable,
                                      src_file_path, src_file_path)

                if files_dict[file_name] not in self._tgt_files:
                    do_it = True

                if do_it:
                    tgt_file_path = path.join(self._tgt_dir,
                                              files_dict[file_name])
                    drawable = pdb.gimp_image_flatten(img)
                    if max_w > 0 or max_h > 0:
                        self.__resize(img)

                    if img.base_type != INDEXED:
                        pdb.gimp_image_convert_precision(img, PRECISION[bpp])
                        if not icc:
                            if img.base_type == RGB:
                                prof_key = 'rgb_export'
                            else:
                                prof_key = 'gray_export'
                            if bpp % 2 == 0:
                                prof_key = '{}_{}'.format(prof_key, 'gamma')
                            else:
                                prof_key = '{}_{}'.format(prof_key, 'linear')

                            profile = DEFAULTS['icc'][prof_key]
                            if profile is not None:
                                pdb.plug_in_icc_profile_apply(img, profile,
                                                              intent, bpc)

                    if strip_meta:
                        pdb.gimp_image_set_metadata(img, '')

                    self._export(img, drawable, tgt_file_path)

            pdb.gimp_image_delete(img)


class AutoJpgExporter(AutoImageExporter):

    def __init__(self, options):
        """JPG exporter sub-class.

        Arguments:
        options (dict): Export options.
        """

        options['ext'] = 'jpg'
        AutoImageExporter.__init__(self, options)

    def _export(self, img, drawable, tgt_path):
        """Export an image in JPG format."""

        pdb.file_jpeg_save(
            img, drawable, tgt_path, tgt_path,
            self._opt['quality'],
            self._opt['blur'],
            self._opt['optimize'],
            self._opt['progressive'],
            self._opt['comment'],
            self._opt['sub-sampling'],
            self._opt['baseline'],
            self._opt['mcu'],
            self._opt['dtc']
        )


class AutoPngExporter(AutoImageExporter):

    def __init__(self, options):
        """Png exporter sub-class.

        Arguments:
        options (dict): Export options.
        """

        options['ext'] = 'png'
        AutoImageExporter.__init__(self, options)

    def _export(self, img, drawable, tgt_path):
        """Export an image in PNG format."""

        pdb.file_png_save2(
            img, drawable, tgt_path, tgt_path,
            self._opt['interlace'],
            self._opt['compression'],
            self._opt['bkgd'],
            self._opt['gama'],
            self._opt['offs'],
            self._opt['phys'],
            self._opt['time'],
            self._opt['comment'],
            self._opt['transparency']
        )


def auto_export_jpg(src_dir, tgt_dir, quality, blur, mode, sampling, profile,
                    intent, bpc, bpp, strip_meta, max_w, max_h):
    """Open XCF files in src_dir and exports them to tgt_dir in the JPG format.

    Arguments:
    src_dir (string, path): Path of the source dir containing xcf images.
    tgt_dir (string, path): Path of the target dir to save the jpg images.
    quality (int): JPG quality factor.
    blur (float): Gaussian blur factor.
    mode (int): Compression mode.
    sampling (int): Color sub-sampling mode.
    profile (string): ICC profile path.
    intent (int): Profile conversion rendering intent.
    bpc (bool): Use black point compensation.
    bpp (int): Promote/demote to this precision.
    strip_meta (bool): Strip metadata flag.
    max_w (int): Maximum width size for the exported images.
    max_h (int): Maximum height size for the exported images.
    """

    baseline = progressive = 0

    if mode == 0:
        progressive = 0
        baseline = 1
    elif mode == 1:
        progressive = 1
        baseline = 0

    options = {
        'src_dir': src_dir,
        'tgt_dir': tgt_dir,
        'quality': quality / 100,
        'blur': blur,
        'optimize': 1,
        'progressive': progressive,
        'comment': '',
        'sub-sampling': sampling,
        'baseline': baseline,
        'mcu': 0,
        'dtc': 2,
        'profile': profile,
        'intent': intent,
        'bpc': bpc,
        'bpp': bpp,
        'strip_meta': strip_meta,
        'max_w': abs(max_w),
        'max_h': abs(max_h)
    }

    exporter = AutoJpgExporter(options)
    exporter.run()


register(
    'auto-export-jpg',
    'Export xcf files to jpg',
    'Export xcf files to jpg',
    'Sergio Jiménez Herena',
    'Sergio Jiménez Herena',
    'March 2018',
    'As JPG files',
    '',
    [
        (PF_DIRNAME, 'src_path', 'XCF sources directory:', DEFAULTS['dir']),
        (PF_DIRNAME, 'tgt_path', 'Export directory:', None),
        (PF_SLIDER, 'quality', 'JPG Quality', 80, (0, 100, 1)),
        (PF_SLIDER, 'blur', 'Gaussian Blur', 0.1, (0, 1, 0.01)),
        (PF_OPTION, 'mode', 'Compression mode',
         0, ('Baseline',
             'Progressive')),
        (PF_OPTION, 'sampling', 'Color Sub-Sampling',
         2, ('4:2:0',
             '4:2:2 (Horizontal)',
             '4:4:4',
             '4:2:2 (Vertical)')),
        (PF_FILE, 'profile', 'Color Profile', None),
        (PF_OPTION, 'intent', 'Rendering intent',
         0, ('Perceptual',
             'Relative Colorimetric',
             'Saturation',
             'Absolute Colorimetric')),
        (PF_BOOL, 'bpc', 'Black Point Compensation', False),
        (PF_OPTION, 'bpp', 'Precision',
         0, ('Unsigned 8bpp sRGB', 'Unsigned 8bpp Linear',
             'Unsigned 16bpp sRGB', 'Unsigned 16bpp Linear',
             'Unsigned 32bpp sRGB', 'Unsigned 32bpp Linear',
             'Float 16bpp sRGB', 'Float 16bpp Linear',
             'Float 32bpp sRGB', 'Float 32bpp Linear')),
        (PF_BOOL, 'strip_meta', 'Strip metadata', True),
        (PF_INT, 'max_w', 'Maximum width (0 = Disabled)', 0),
        (PF_INT, 'max_h', 'Maximum height (0 = Disabled)', 0)
    ],
    [],
    auto_export_jpg,
    menu='<Image>/Automation/Export directory'
)


def auto_export_png(src_dir, tgt_dir, compression, interlace, transparency,
                    bkgd, gama, offs, phys, time, comment, profile,
                    intent, bpc, bpp, strip_meta, max_w, max_h):
    """Open XCF files in src_dir and exports them to tgt_dir in the PNG format.

    Arguments:
    compression (int): Compression factor.
    interlace (int): Adam 7 Interlacing flag.
    transparency (int): Preserve transparent pixels flag.
    bkgd (int): Export bKGD chunk.
    gama (int): Save gAMA chunk.
    offs (int): Save oFFS chunk.
    phys (int): Save pHYS chunk.
    time (int): Save tIME chunk.
    comment (int): Write comment flag.
    profile (string): ICC profile path.
    intent (int): Profile conversion rendering intent.
    bpc (bool): Use black point compensation.
    bpp (int): Promote/demote to this precision.
    strip_meta (bool): Strip metadata flag.
    max_w (int): Maximum width size for the exported images.
    max_h (int): Maximum height size for the exported images.
    """

    options = {
        'src_dir': src_dir,
        'tgt_dir': tgt_dir,
        'compression': compression,
        'interlace': interlace,
        'transparency': transparency,
        'bkgd': bkgd,
        'gama': gama,
        'offs': offs,
        'phys': phys,
        'time': time,
        'comment': comment,
        'profile': profile,
        'intent': intent,
        'bpc': bpc,
        'bpp': bpp,
        'strip_meta': strip_meta,
        'max_w': max_w,
        'max_h': max_h
    }

    exporter = AutoPngExporter(options)
    exporter.run()


register(
    'python_fu_auto_export_png',
    'Export xfc files to png',
    'Export xfc files to png',
    'Sergio Jiménez Herena',
    'Sergio Jiménez Herena',
    'March 2018',
    'As PNG files',
    '',
    [
        (PF_DIRNAME, 'src_path', 'XCF sources directory:', DEFAULTS['dir']),
        (PF_DIRNAME, 'tgt_path', 'Export directory:', None),
        (PF_SLIDER, 'compression', 'Compression', 9, (0, 9, 1)),
        (PF_BOOL, 'interlace', 'Adam 7 Interlacing', True),
        (PF_BOOL, 'transparency', 'Preserve transparent pixels', True),
        (PF_BOOL, 'bkgd', 'Write bKGD chunk', True),
        (PF_BOOL, 'gama', 'Write gAMA chunk', True),
        (PF_BOOL, 'offs', 'Write oFFS chunk', True),
        (PF_BOOL, 'phys', 'Write pHYS chunk', True),
        (PF_BOOL, 'time', 'Write tIME chunk', True),
        (PF_BOOL, 'comment', 'Write comment', True),
        (PF_FILE, 'profile', 'Color Profile', None),
        (PF_OPTION, 'intent', 'Rendering intent',
         0, ('Perceptual',
             'Relative Colorimetric',
             'Saturation',
             'Absolute Colorimetric')),
        (PF_BOOL, 'bpc', 'Black Point Compensation', False),
        (PF_OPTION, 'bpp', 'Precision',
         0, ('Unsigned 8bpp sRGB', 'Unsigned 8bpp Linear',
             'Unsigned 16bpp sRGB', 'Unsigned 16bpp Linear',
             'Unsigned 32bpp sRGB', 'Unsigned 32bpp Linear',
             'Float 16bpp sRGB', 'Float 16bpp Linear',
             'Float 32bpp sRGB', 'Float 32bpp Linear')),
        (PF_BOOL, 'strip_meta', 'Strip metadata', True),
        (PF_INT, 'max_w', 'Maximum width (0 = Disabled)', 0),
        (PF_INT, 'max_h', 'Maximum height (0 = Disabled)', 0)
    ],
    [],
    auto_export_png,
    menu='<Image>/Automation/Export directory'
)
