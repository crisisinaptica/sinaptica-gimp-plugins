#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_auto_update.py

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

    Modifications by Sinaptica.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import division
from auto_base import *
from urlparse import urlparse, urlunparse
from os import remove

__author__    = 'Stephen Kiel, Sergio Jiménez Herena'
__copyright__ = 'Stephen Kiel'
__license__   = 'GPL3'


def update_image(img, compress):
    """Execute the current work flow step and update the images.

    The WorkFlow Step commands are pulled from a 'tree' which is accessed
    through the AutoFlowReader class.

    Arguments:
    image (GimpImage): A gimp.Image object
    compress (bool): Save a compressed xcf
    """

    flow_read = AutoFlowReader()

    update_flag   = bool(int(img.parasite_find('update_flag').data))
    overwrite_xcf = bool(int(img.parasite_find('overwrite_xcf').data))
    flow          = str(img.parasite_find('flow'))
    next_step     = str(img.parasite_find('next_step'))

    file_uri          = pdb.gimp_image_get_xcf_uri(img)
    unparsed_file_uri = urlparse(file_uri)
    file_path         = unparsed_file_uri.path

    if update_flag:
        command_list, new_next_step = flow_read.extract(flow, next_step)
        for cmd in command_list:
            exec cmd
        img.attach_new_parasite('current_step', PARAS_IDX, next_step)
        img.attach_new_parasite('next_step', PARAS_IDX, new_next_step)
        img.attach_new_parasite('update_flag', PARAS_IDX, '0')

    if update_flag or overwrite_xcf:
        active_layer = pdb.gimp_image_get_active_layer(img)
        comp = DEFAULTS['compression']
        if compress:
            if path.splitext(file_path)[1] == '.xcf':
                remove(file_path)
                file_path = '{}.{}{}'.format(path.splitext(file_path)[0],
                                             'xcf', comp)
            if comp == 'gz':
                pdb.file_gz_save(img, active_layer,
                                 file_path, file_path)
            elif comp == 'bz2':
                pdb.file_bz2_save(img, active_layer,
                                  file_path, file_path)
            elif comp == 'xz':
                pdb.file_xz_save(img, active_layer,
                                 file_path, file_path)
        else:
            if path.splitext(file_path)[1] == '.xcf' + comp:
                remove(file_path)
                file_path = '{}.{}'.format(path.splitext(file_path)[0],
                                           'xcf')

            file_uri = urlunparse((unparsed_file_uri.scheme,
                                   unparsed_file_uri.netloc,
                                   file_path,
                                   unparsed_file_uri.params,
                                   unparsed_file_uri.query,
                                   unparsed_file_uri.fragment))

            pdb.gimp_xcf_save(0, img, active_layer, file_uri, file_path)

        pdb.gimp_image_clean_all(img)


def auto_auto_update_images(compress):
    """Update all currently opened images.

    Arguments:
    compress (bool): Save a compressed xcf.
    """

    for img in gimp.image_list():
        update_image(img, compress)


def auto_auto_update_dir(src_dir, compress):
    """Update all files in a directory.

    Creates a list of all of the xcf files in a selected directory and calls
    the function 'update_images' with each filename in that list.

    Arguments:
    src_dir (string, path): Directory containing images to update.
    """

    pdb.gimp_register_file_handler_uri('python-fu-auto_auto_update_dir')

    src_file_list = [path.join(src_dir, f) for f in listdir(src_dir) if
                     path.splitext(f)[1] == '.xcf' or
                     path.splitext(f)[1] == '.xcfgz']

    for file_name in src_file_list:
        img = pdb.gimp_file_load(file_name, file_name)
        update_image(img, compress)
        pdb.gimp_image_delete(img)


register(
    "python_fu_auto_auto_update_images",
    "Auto update all currently opened images",
    "Auto update all currently opened images",
    "Sergio Jiménez Herena",
    "Sergio Jiménez Herena",
    "Oct 2018",
    "3a) Auto Update Images",
    "",
    [
        (PF_BOOL, "compress", "Compress XCF files", False)
    ],
    [],
    auto_auto_update_images,
    menu="<Image>/Automation"
)

register(
    "python_fu_auto_auto_update_dir",
    "Auto update a directory of xcf images",
    "Auto update a directory of xcf images",
    "Stephen Kiel, Sergio Jiménez Herena",
    "Stephen Kiel",
    "Aug 2013",
    "3b) Auto Update Folder",
    "",
    [
        (PF_DIRNAME, "src_path", "XCF directory:", DEFAULTS['dir']),
        (PF_BOOL, "compress", "Compress XCF files", False)
    ],
    [],
    auto_auto_update_dir,
    menu="<Image>/Automation"
)
