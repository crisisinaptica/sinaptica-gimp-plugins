#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_commander.py

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

    Modifications by Sinaptica.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import division
from auto_base import *

__author__    = 'Stephen Kiel, Sergio Jiménez Herena'
__copyright__ = 'Copyright 2013, Stephen Kiel'
__license__   = 'GPL3'

try:
    cmdr_read = AutoCmdrReader()
    macro_names = cmdr_read.get_names()
except IOError:
    macro_names = []


def execute_commands(img, command_list):
    """Execute the macro command list on an image.

    Arguments:
    image (int): An image id.
    command_list (list): The list of commands to be executed.
    """

    pdb.gimp_image_undo_group_start(img)
    for cmd in command_list:
        exec cmd
    pdb.gimp_image_undo_group_end(img)


def auto_commander(image, macro_name, do_for_all):
    """Display a prompt to select a macro sequence by name.

    Use the name to locate the set of commands associated with that macro in
    the commander tree.
    Form a list from the set, and run that list of commands on the image.

    Arguments:
    image (int): Current image id.
    macro_name (string): Name of the macro to execute.
    """

    pdb.gimp_plugin_enable_precision()

    commander_name = macro_names[macro_name]
    command_list = cmdr_read.get_cmd_list(commander_name)

    if do_for_all:
        image_list = gimp.image_list()
        for image in image_list:
            execute_commands(image, command_list)
    else:
        execute_commands(image, command_list)


register(
    "python_fu_auto_commander",
    "Commander",
    "Commander",
    "Stephen Kiel, Sergio Jiménez Herena",
    "Stephen Kiel",
    "August 2013",
    "Commander - Command Sequencer",
    "*",
    [
        (PF_IMAGE, "image", "Input image", None),
        (PF_OPTION, "macro_name", "Select a command", 0, macro_names),
        (PF_BOOL, 'do_for_all', 'All opened images', False)
    ],
    [],
    auto_commander,  # Defined function
    menu="<Image>/Automation"  # Menu Location
)
