#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
auto_base.py

    Part of a set of scripts to Automate the Editing of images with Gimp

    Based on the original scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

    Modifications by Sinaptica.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import print_function
import xml.etree.ElementTree as eT
from os import path, listdir, makedirs
from re import compile
from json import load
from gimpfu import *

__author__    = 'Stephen Kiel, Sergio Jiménez Herena'
__copyright__ = 'Copyright 2013, Stephen Kiel'
__license__   = 'GPL3'

PLUGIN_DIR = path.join(gimp.directory, 'plug-ins', 'automation')

PARAS_IDX = 5

PRECISION = {
    0:  PRECISION_U8_GAMMA,
    1:  PRECISION_U8_LINEAR,
    2:  PRECISION_U16_GAMMA,
    3:  PRECISION_U16_LINEAR,
    4:  PRECISION_U32_GAMMA,
    5:  PRECISION_U32_LINEAR,
    6:  PRECISION_HALF_GAMMA,
    7:  PRECISION_HALF_LINEAR,
    8:  PRECISION_FLOAT_GAMMA,
    9:  PRECISION_FLOAT_LINEAR,
    10: PRECISION_DOUBLE_GAMMA,
    11: PRECISION_DOUBLE_LINEAR
}

JSON_PATH = path.abspath(path.join(PLUGIN_DIR, 'automation.json'))

if path.isfile(JSON_PATH):
    DEFAULTS = load(open(JSON_PATH))
else:
    DEFAULTS = {
        'dir': None,
        'import_bpp': 0,
        'export_bpp': 0,
        'icc': {
            'rgb_working_gamma':   None,
            'rgb_working_linear':  None,
            'rgb_export_gamma':    None,
            'rgb_export_linear':   None,
            'gray_working_gamma':  None,
            'gray_working_linear': None,
            'gray_export_gamma':   None,
            'gray_export_linear':  None
        },
        'ext': ['.jpg', '.jpeg', '.png', '.tif', '.tiff'],
        'compression': 'gz'
    }


class AutoXml(object):
    """Base class for AutoXmlReader and AutoXmlGenerator."""

    def __init__(self):

        self._src_type = ''
        self._base_path = path.join(PLUGIN_DIR, 'auto_xml')
        self._xml_path = ''

    def _set_path(self):

        if self._src_type == 'flow':
            self._xml_path = path.join(self._base_path,
                                       'flow',
                                       'combined_flow.xml')
        elif self._src_type == 'prop':
            self._xml_path = path.join(self._base_path,
                                       'property',
                                       'flag_properties.xml')
        elif self._src_type == 'cmdr':
            self._xml_path = path.join(self._base_path,
                                       'commander',
                                       'combined_commander.xml')

    def get_path(self):
        """Returns the path of the xml file the method is acting upon."""

        return self._xml_path


class AutoXmlReader(AutoXml):
    """Base class for AutoCmdrReader, AutoPropReader and AutoFlowReader."""

    def __init__(self):

        AutoXml.__init__(self)

    def _reader(self):

        self._set_path()
        return eT.parse(self.get_path())

    def _lev1_list(self):

        lev1_list = []
        tree = self._reader()
        for l1 in tree.getroot():
            lev1_list.append(l1.text)
        return lev1_list

    def _lev2_list(self):

        lev2_list = []
        tree = self._reader()
        for l1 in tree.getroot():
            for l2 in l1:
                lev2_list.append(l2.text)
        return lev2_list


class AutoCmdrReader(AutoXmlReader):
    """Sub-Class to read from the combined_commander.xml file."""

    def __init__(self):

        AutoXmlReader.__init__(self)
        self._src_type = 'cmdr'
        self._tree = self._reader()

    def get_names(self):
        """Get the names of all defined macros.

        Can be used to populate a list for a menu in the user interface.

        Returns:
        names (list) : Macro names fetched from the xml file.
        """

        names = self._lev1_list()
        names.sort()

        return names

    def get_cmd_list(self, mac_name):
        """Get the list of commands associated with the macro.

        This is the list of commands the commander will pass to exec()

        Parameters:
        mac_name (string): Name of the macro.

        Returns:
        cmd_list (list): List of commands associated to the macro.
        """

        cmd_list = []

        for item in self._tree.getroot():
            if item.text == mac_name:
                for command in item:
                    if command.tag == 'command':
                        cmd_list.append(command.text)

        return cmd_list


class AutoPropReader(AutoXmlReader):
    """Sub-Class to read from the flag_properties.xml file."""

    def __init__(self):

        AutoXmlReader.__init__(self)
        self._src_type = 'prop'
        self._tree = self._reader()

    def get_tree(self):

        return self._tree

    def get_tree_root(self):

        return self._tree.getroot()

    def get_names(self):
        """Get the names of all defined properties.

        Returns:
        prop_list (list): Property names
        """

        prop_list = self._lev2_list()
        return prop_list

    def get_options(self, prop_name):
        """Get the options for a property.

        Parameters:
        prop_name (string): Property name

        Returns:
        options (list): Options for the given property.
        """

        options = []

        for top in self.get_tree_root():
            for item in top:
                if item.text == prop_name:
                    for option in item:
                        if option.tag == 'option':
                            options.append(option.text)

        return options

    def get_default_options(self):
        """Get the default options for each defined property.

        Returns:
        default_options (list): A list containing one tuple for each defined
        option in the form ('option', 'default_value')
        """

        default_options = []

        for top in self._tree.getroot():
            for item in top:
                prop_name = item.text
                for default in item:
                    if default.tag == 'default':
                        name_val = (prop_name, default.text)
                        default_options.append(name_val)

        return default_options


class AutoFlowReader(AutoXmlReader):
    """Sub-Class to read from the combined_flow.xml file."""

    def __init__(self):

        AutoXmlReader.__init__(self)
        self._src_type = 'flow'
        self._tree = self._reader()

    def get_tree(self):

        return self._tree

    def get_tree_root(self):

        return self._tree.getroot()

    def get_names(self):
        """Get the names of all defined work flows.

        Returns:
        flow_names (list): List of defined work flows.
        """

        flow_names = self._lev1_list()
        return flow_names

    def extract(self, flow_name, step_match):
        """Get the commands associated with a 'step' in a defined work flow.

        Returns a list composed of the commands associated with 'step_match'
        in the given 'flow_name' workflow and the name of the 'next step'.

        Arguments:
        flow_name (string): The name of the work flow.
        step_match (string): The name of the step to work upon.

        Returns:
        commands (list): List of commands associated with the given 'flow_name'
        work flow and 'step_match' step.
        next_step (string): The name of the step that follows 'step_match' in
        the 'flow_name' work flow.
        """

        commands = []
        next_step = 'FINISHED'

        get_next_step = False

        for item in self.get_tree_root():
            if item.text == flow_name:
                for step_name in item:
                    if step_name.text == step_match:
                        for command in step_name:
                            if command.tag == 'command':
                                commands.append(command.text)
                    if get_next_step:
                        next_step = step_name.text
                        get_next_step = False
                    if step_name.text == step_match:
                        get_next_step = True

        return commands, next_step

    def first_name_dict(self):

        first_name_dict = {}
        tree = self._reader()
        tree_root = tree.getroot()

        for flow_name in tree_root:
            first_name_dict[flow_name.text] = flow_name[0].text

        return first_name_dict


class AutoXmlGenerator(AutoXml):

    def __init__(self):
        """Sub-Class to parse and combine the *.def files into the xml files.

        _file_ext (string): Common file extension for all pseudo code files
        _root_list (list): Tags for the root level of the tree
        _lev1_list (list): Tags for the 1st level of hierarchy under tree root
        _lev2_list (list): Tags for the 2nd level of hierarchy under tree root
        """

        AutoXml.__init__(self)
        self._file_ext  = '.def'
        self._root_list = []
        self._lev1_list = []
        self._lev2_list = []

    def __parse_tag(self, line):
        """Split a line into 'tags' and 'tails'.

        The tags will be the xml identifiers or enclosures, and the tails will
        be the associated text. If a line is blank or starts with a '#'  it
        will be ignored and skipped.
        """

        if len(line) > 0 and line[0] != '#':

            tag = line[0:line.find('>')]
            tag = tag.lower()
            tail = line[line.find('>') + 1:]

            if tag in self._root_list:
                level = 'root'
            elif tag in self._lev1_list:
                level = 'level1'
            elif tag in self._lev2_list:
                level = 'level2'
            else:
                level = 'none'

            return tag, tail, level

        else:

            return '', '', ''

    def __pseudo_to_xml(self, input_pseudo):
        """Read pseudo code from a file to a data structure.

        The generated tree will have a root with two levels of sub hierarchy.

        Arguments:
        input_pseudo (string, path): Pseudo code file path to parse.

        Return:
        tree (ElementTree): Structured and formatted data from the pseudo code.
        """

        infile = open(input_pseudo)

        el_root = el_lev1 = el_lev2 = None

        # convert '>>>' alias for 'command>'
        xform = compile('^>>>\s*')

        for line in infile:
            line = line.strip()
            line = xform.sub('command>', line)

            tag, tail, level = self.__parse_tag(line)

            # Build and format line into the tree.

            if level == 'root':
                el_root      = eT.Element(tag)
                el_root.text = tail
                el_root.tail = '\n  '

            elif level == 'level1' and el_root is not None:
                el_lev1      = eT.SubElement(el_root, tag)
                el_lev1.text = tail
                el_lev1.tail = '\n    '

            elif level == 'level2' and el_lev1 is not None:
                el_lev2      = eT.SubElement(el_lev1, tag)
                el_lev2.text = tail
                el_lev2.tail = '\n      '

        tree = eT.ElementTree(el_root)

        return tree

    def __gen_tree(self):
        """Reads all definition files and combine the data.

        Read all of the *.def files in the named xml directory.
        For each flow *.def pseudo code file, read it into a tree.
        Append the flow xml trees together into a combined tree.
        """

        file_path = self.get_path()
        file_dir = path.dirname(file_path)
        root = eT.Element('combined')
        root.text = 'Definition' + '\n  '

        try:
            file_list = listdir(file_dir)
            for file_name in file_list:
                if file_name.count(self._file_ext) > 0:
                    file_name = path.join(file_dir, file_name)
                    sub_tree = self.__pseudo_to_xml(file_name)
                    sub_node = sub_tree.getroot()
                    root.append(sub_node)
            tree = eT.ElementTree(root)
            tree.write(file_path)

        except OSError:
            print('*** Could not find {}'.format(file_dir))

    def gen_commander(self):
        """Write the combined_commander xml file."""

        self._src_type  = 'cmdr'
        self._set_path()
        self._root_list = ['commander']
        self._lev1_list = ['command', 'comment']
        self._lev2_list = []
        self.__gen_tree()

    def gen_flow(self):
        """Write the combined_flow xml file."""

        self._src_type  = 'flow'
        self._set_path()
        self._root_list = ['flow']
        self._lev1_list = ['step']
        self._lev2_list = ['command', 'comment']
        self.__gen_tree()

    def gen_property(self):
        """Write the flag_properties xml file."""

        self._src_type  = 'prop'
        self._set_path()
        self._root_list = ['flags']
        self._lev1_list = ['property']
        self._lev2_list = ['default', 'option', 'comment']
        self.__gen_tree()


class AutoTestBench(object):
    """Exercises the classes AutoXmlGenerator and AutoXmlReader."""

    @staticmethod
    def print_class_docs():
        print('*** AutoXmlGenerator\n{}\n\n'
              '*** AutoCmdrReader\n{}\n\n'
              '*** AutoFlowReader\n{}\n\n'
              '*** AutoPropReader\n{}\n\n'
              '*** AutoTestBench\n {}\n'.format(AutoXmlGenerator.__doc__,
                                                AutoCmdrReader.__doc__,
                                                AutoFlowReader.__doc__,
                                                AutoPropReader.__doc__,
                                                AutoTestBench.__doc__)
              )

    @staticmethod
    def test_xml_gen():

        xml_obj = AutoXmlGenerator()
        print('\n*** Testing the AutoXmlGenerator Class\n')
        xml_obj.gen_commander()
        print('\tWrote \'{}\''.format(xml_obj.get_path()))
        xml_obj.gen_flow()
        print('\tWrote \'{}\''.format(xml_obj.get_path()))
        xml_obj.gen_property()
        print('\tWrote \'{}\'\n'.format(xml_obj.get_path()))
        print('\tCheck the time/date stamp of the xml files.\n'
              '\tCheck the correctness of the content by exercising '
              'the test_xml_read method which reads the xml and '
              'prints the output of each method.\n')

    @staticmethod
    def test_xml_read():

        print('\n*** Testing the AutoCmdrReader Class\n')
        read_obj = AutoCmdrReader()

        print('**  AutoCmdrReader get_names():\n{}\n'
              .format(read_obj.get_names()))
        print('**  AutoCmdrReader get_cmd_list(\'Add Color Layer\'):\n{}'
              .format(read_obj.get_cmd_list('Centered Grid')))

        print('\n\n*** Testing the AutoPropReader Class\n')
        read_obj = AutoPropReader()

        print('**  AutoPropReader get_names():\n{}\n'
              .format(read_obj.get_names()))

        print('**  AutoPropReader tree:')
        for l1 in read_obj.get_tree_root():
            for l2 in l1:
                print(l2.text)
                for l3 in l2:
                    if l3.tag == 'default':
                        print('\tdefault value =>  {}'.format(l3.text))
                    if l3.tag == 'option':
                        print('\toption value  ->  {}'.format(l3.text))

        print('** AutoPropReader get_options(\'update_flag\'):\n{}\n'
              '\n** AutoPropReader get_default_options():\n{}\n'
              .format(read_obj.get_options('update_flag'),
                      read_obj.get_default_options()))

        print('\n\n*** Testing the AutoFlowReader Class\n')
        read_obj = AutoFlowReader()

        print('**  AutoFlowReader get_names():\n{}\n'
              .format(read_obj.get_names()))

        print('**  AutoFlowReader tree:')
        for flow_name in read_obj.get_tree_root():
            print('\n{}'.format(flow_name.text))
            for flow_step in flow_name:
                print('step -> ' + flow_step.text)
                for command in flow_step:
                    if command.tag == 'command':
                        print('  command => ' + command.text)
                    if command.tag == 'comment':
                        print('  comment -- ' + command.text)

        cmd_list, next_step = read_obj.extract('Standard', 'DynamicRange')
        print('\n\n'
              '**  AutoFlowReader extract() Next Name:\n{}\n\n'
              '**  AutoFlowReader extract() Command List:\n{}\n\n'
              '**  AutoFlowReader first_name_dic():\n{}\n\n'
              .format(next_step, cmd_list, read_obj.first_name_dict()))
