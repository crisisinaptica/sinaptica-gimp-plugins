#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
automation.py
    by Sinaptica.

    Part of a set of scripts to automate the editing of images with Gimp

    Based on scripts and ideas by Stephen Kiel
    See https://www.gimp.org/tutorials/Automate_Editing_in_GIMP/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from auto_write_xml import *
from auto_rw_parasites import *
from auto_import_img import *
from auto_auto_update import *
from auto_export_img import *
from auto_commander import *

__author__    = 'Sergio Jiménez Herena'
__copyright__ = 'Sergio Jiménez Herena'
__license__   = 'GPL3'

main()
