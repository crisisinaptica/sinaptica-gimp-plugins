#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
doom_sprite_composer.py
    Doom Sprite Composer GIMP plug-in
    by Sinaptica


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import print_function, division
from os import path as os_path
from re import match
from gimpfu import *

__author__ = 'Sergio Jiménez Herena'
__copyright__ = '2017, Sergio Jiménez Herena'
__license__ = 'GPL3'


def has_zero_pixels(curr_image, file_path):

    temp_layer = pdb.gimp_file_load_layer(curr_image, file_path)
    curr_image.add_layer(temp_layer, -1)
    pixel_count = pdb.gimp_drawable_histogram(temp_layer,
                                              HISTOGRAM_VALUE,
                                              0, 1)[3:4][0]
    curr_image.remove_layer(temp_layer)

    if pixel_count == 0:
        return True
    else:
        return False


def doom_sprite_composer(src_dir, tgt_dir,
                         camera_set, crop, padding, palette, colorize,
                         grad1, grad2, grad3, grad4,
                         grad5, grad6, grad7, grad8):

    pdb.gimp_context_set_antialias(False)

    total_sprites, sprite_paths = pdb.file_glob(
        os_path.join(src_dir, "*_????_C??.png"), 0)

    sprite_paths = list(sprite_paths)
    sprite_paths.sort()

    if total_sprites == 0:
        pdb.gimp_message("No sprites found.")
        return

    mask_glob = os_path.join(src_dir, "*_????_C??_M??.png")
    mask_paths = list(pdb.file_glob(mask_glob, 0)[-1])
    mask_paths.sort()

    temp_img = pdb.gimp_file_load(sprite_paths[0], 'size_check')
    sprite_w, sprite_h = (temp_img.width, temp_img.height)
    pdb.gimp_image_delete(temp_img)

    sheet_cols = {0: 5, 1: 8, 2: 16}[camera_set]
    sheet_rows = total_sprites // sheet_cols
    sheet_w, sheet_h = (sprite_w * sheet_cols, sprite_h * sheet_rows)

    sheet = gimp.Image(sheet_w, sheet_h, RGB)

    mask_paths = [path for path in mask_paths
                  if not has_zero_pixels(sheet, path)]

    channels = []
    gradients = (grad1, grad2, grad3, grad4, grad5, grad6, grad7, grad8)

    for channel in range(0, int(len(mask_paths) // total_sprites)):

        new_channel = pdb.gimp_channel_new(
            sheet, sheet_w, sheet_h,
            'mask_{}'.format(str(channel+1).zfill(2)),
            100, (0, 0, 0)
        )
        new_channel.visible = False
        sheet.add_channel(new_channel, channel)
        channels.append(new_channel)

    for sprite_idx, sprite_path in enumerate(sprite_paths):

        temp_layer = pdb.gimp_file_load_layer(sheet, sprite_path)
        sheet.add_layer(temp_layer, -1)

        offset_x = sprite_idx % sheet_cols * sprite_w
        offset_y = sprite_idx // sheet_cols * sprite_h

        temp_layer.set_offsets(offset_x, offset_y)

        masks_imported = sprite_idx * len(channels)
        masks_to_import = masks_imported + len(channels)

        for mask_idx, mask in enumerate(range(masks_imported,
                                              masks_to_import)):
            temp_layer = pdb.gimp_file_load_layer(sheet, mask_paths[mask])
            temp_layer.set_offsets(offset_x, offset_y)
            sheet.add_layer(temp_layer, -1)

            sheet.active_layer = temp_layer
            sheet.active_channel = channels[mask_idx]
            pdb.gimp_image_select_item(sheet, CHANNEL_OP_ADD, temp_layer)
            pdb.gimp_edit_fill(channels[mask_idx], 2)

            pdb.gimp_selection_none(sheet)
            sheet.remove_layer(temp_layer)

    merged_sprites = pdb.gimp_image_merge_visible_layers(sheet, CLIP_TO_IMAGE)
    basename = '{}_sheet'.format(
        match('\w*(?=_[0-9]{4})', os_path.basename(sprite_paths[0])).group()
    )

    merged_sprites.name = basename
    sheet.active_layer = merged_sprites

    if crop:
        pdb.plug_in_zealouscrop(sheet, merged_sprites, padding, 1)
        pass

    if colorize:

        for gradient, channel in enumerate(channels):
            mask_layer = gimp.Layer(
                sheet,
                'Gradient mask {}'.format(str(gradients[gradient])),
                int(sheet_w + padding * 2),
                int(sheet_h + padding * 2),
                RGB_IMAGE,
                100.0,
                LAYER_MODE_NORMAL
            )

            sheet.add_layer(mask_layer, -1)
            pdb.gimp_drawable_fill(mask_layer, FILL_WHITE)
            pdb.gimp_invert(mask_layer)

            gimp.context_set_gradient(gradients[gradient])
            pdb.gimp_image_select_item(sheet, CHANNEL_OP_REPLACE, channel)
            pdb.plug_in_gradmap(sheet, merged_sprites)

            temp_layer = pdb.gimp_layer_new_from_drawable(channel, sheet)
            sheet.add_layer(temp_layer, -1)

            temp_layer.visible = True
            temp_layer = pdb.gimp_image_merge_down(sheet,
                                                   temp_layer,
                                                   CLIP_TO_IMAGE)
            temp_layer.visible = False

    pdb.gimp_selection_none(sheet)

    if palette:

        pdb.gimp_image_convert_indexed(sheet, CONVERT_DITHER_NONE,
                                       CONVERT_PALETTE_CUSTOM, 256,
                                       FALSE, FALSE, palette)

    try:

        gimp.Display(sheet)

    except gimp.error:

        file_name = '{}.xcf'.format(basename)
        sheet.filename = file_name
        save_path = os_path.join(tgt_dir, file_name)
        pdb.gimp_xcf_save(0, sheet, None, save_path, file_name)
        pdb.gimp_image_delete(sheet)

    return


register(
    "doom_sprite_composer",
    "Doom Sprite Composer.",
    "Merge and optionally colorize and crop a set of sprites.",
    "Sergio Jiménez Herena",
    "Sergio Jiménez Herena",
    "2017",
    "Doom Sprite Composer...",
    "",
    [
        (PF_DIRNAME, "src_dir", "Source directory", "/path/to/source"),
        (PF_DIRNAME, "tgt_dir", "Target directory (Non Interactive Execution)",
         "/path/for/output"),
        (PF_OPTION, "camera_set", "Camera set", 0, ("half", "full", "xtra")),
        (PF_OPTION, "crop", "Apply zealous crop", 1, ("no", "yes")),
        (PF_SPINNER, "padding", "Crop Padding", 5, (0, 200, 1)),
        (PF_PALETTE, "palette", "Palette", None),
        (PF_OPTION, "colorize", "Apply gradients to masks", 0, ("no", "yes")),
        (PF_GRADIENT, "grad1", "Mask 1 Grad", None),
        (PF_GRADIENT, "grad2", "Mask 2 Grad", None),
        (PF_GRADIENT, "grad3", "Mask 3 Grad", None),
        (PF_GRADIENT, "grad4", "Mask 4 Grad", None),
        (PF_GRADIENT, "grad5", "Mask 5 Grad", None),
        (PF_GRADIENT, "grad6", "Mask 6 Grad", None),
        (PF_GRADIENT, "grad7", "Mask 7 Grad", None),
        (PF_GRADIENT, "grad8", "Mask 8 Grad", None)
    ],
    [],
    doom_sprite_composer,
    menu="<Image>/File/Create/Acquire"
)

main()
