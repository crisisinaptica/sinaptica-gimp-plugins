# -*- coding: utf-8 -*-

"""
Watermark GIMP plug-in

Create a watermark layer.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

from __future__ import division
from gimpfu import *

__author__    = 'Sergio Jiménez Herena'
__copyright__ = '2018, Sergio Jiménez Herena'
__license__   = 'GPL3'


def watermark(image, text, font, size, color, opacity):
    w_layer = pdb.gimp_text_layer_new(image, text, font, size, 0)
    pdb.gimp_image_insert_layer(image, w_layer, None, 0)
    offset_x = int(round(image.width / 2 - w_layer.width / 2))
    offset_y = int(round(image.height / 2 - w_layer.height / 2))
    pdb.gimp_layer_translate(w_layer, offset_x, offset_y)
    pdb.gimp_text_layer_set_color(w_layer, color)
    w_layer.name = 'watermark'
    w_layer.opacity = opacity


register(
    'python-fu-watermark',
    'Create a watermark layer.',
    'Create a watermark layer.',
    __author__,
    __author__,
    '2018',
    'Watermark...',
    '*',
    [
        (PF_IMAGE, "image", "Image", None),
        (PF_STRING, "text", "Text", "Watermark"),
        (PF_FONT, "font", "Font", 'Sans-Serif'),
        (PF_FLOAT, "size", "Size", 64.0),
        (PF_COLOR, "color", "Color", '#FFFFFF'),
        (PF_FLOAT, "opacity", "Opacity", 50.0)
    ],
    [],
    watermark,
    menu='<Image>/Filters/Generic'
)

main()
